use azalea::prelude::*;
use std::env::env;

#[tokio::main]
async fn main() {
    let account = Account::offline("krinet");
    let port = env!("MC_BOT_PORT");
    let url = format!("localhost:{port}");
    let e = azalea::ClientBuilder::new()
            .set_handler(handle)
            .start(account, url)
            .await;
    eprintln!("{:?}", e);
}

#[derive(Default, Clone, Component)]
pub struct State {}

async fn handle(bot: Client, event: Event, state: State) -> anyhow::Result<()> {
    match event {
        Event::Chat(m) => {
            println!("{}", m.message().to_ansi());
        }
        _ => {}
    }

    Ok(())
}